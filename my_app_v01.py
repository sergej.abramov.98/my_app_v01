import aiohttp
import asyncio

async def exchange_info() -> dict:
    while True:
        try:
            async with aiohttp.ClientSession() as session:
                async with session.get('https://api.binance.com/api/v3/exchangeInfo') as response:
                    result = await response.json()
                    await session.close()
                    return result

        except Exception as e:
            print(e)


async def main() -> None:
    exchangeinfo = await exchange_info()
    print(exchangeinfo)
    await asyncio.sleep(1)
    file = open('exchangeinfo.txt', 'w')
    for key, value in exchangeinfo.items():
        file.write(f'{key}, {value}\n')
    file.close()

if __name__ == '__main__':
    asyncio.run(main())